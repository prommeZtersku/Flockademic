'use strict';

// We merely need the type definitions here:
// tslint:disable-next-line:no-implicit-dependencies
import { APIGatewayEvent, Callback, Context, Handler } from 'aws-lambda';

import {
  PostCreatePeriodicalRequest,
  PostGenerateUploadUrlForScholarlyArticleRequest,
  PostInitialiseScholarlyArticleRequest,
  PostMakePeriodicalPublicRequest,
  PostPublishScholarlyArticleRequest,
  PostSubmitScholarlyArticleRequest,
  PostUpdatePeriodicalRequest,
} from '../../../lib/interfaces/endpoints/periodical';

import { initialise, Request } from '../../../lib/lambda/faas';
import { withDatabase } from '../../../lib/lambda/middleware/withDatabase';
import { withSession } from '../../../lib/lambda/middleware/withSession';

import { fetchDashboard } from './resources/fetchDashboard';
import { fetchPeriodical } from './resources/fetchPeriodical';
import { fetchPeriodicalContents } from './resources/fetchPeriodicalContents';
import { fetchPeriodicals } from './resources/fetchPeriodicals';
import { fetchScholarlyArticle } from './resources/fetchScholarlyArticle';
import { fetchScholarlyArticles } from './resources/fetchScholarlyArticles';
import { generateArticleUploadUrl } from './resources/generateArticleUploadUrl';
import { generateAssetUploadUrl } from './resources/generateAssetUploadUrl';
import { initialisePeriodical } from './resources/initialisePeriodical';
import { initialiseScholarlyArticle } from './resources/initialiseScholarlyArticle';
import { isPeriodicalSlugAvailable } from './resources/isPeriodicalSlugAvailable';
import { makePublic } from './resources/makePublic';
import { publishScholarlyArticle } from './resources/publishScholarlyArticle';
import { submitScholarlyArticle } from './resources/submitScholarlyArticle';
import { updatePeriodical } from './resources/updatePeriodical';
import { updateScholarlyArticle } from './resources/updateScholarlyArticle';

// If we're running on Heroku, use the database attached to it in the accounts schema.
// The else clause can be removed once we've fully migrated to Heroku.
const connectionString = (process.env.DATABASE_URL)
  /* istanbul ignore next */
  ? `${process.env.DATABASE_URL}?searchpath=periodicals`
  // tslint:disable-next-line:max-line-length
  : `postgres://${process.env.database_username}:${process.env.database_password}@${process.env.database_endpoint}/periodicals_db`;

export const handler: Handler = (event: APIGatewayEvent, _: Context, callback?: Callback) => {
  const faas = initialise(event, callback);

  faas.get('/', withDatabase(connectionString, 'periodicals', fetchPeriodicals));
  faas.get('/dashboard', withDatabase(connectionString, 'periodicals', withSession(fetchDashboard)));
  faas.get(
    '/articles',
    withDatabase(connectionString, 'periodicals', fetchScholarlyArticles),
  );
  faas.get(
    '/articles/:articleId',
    withDatabase(connectionString, 'periodicals', withSession(fetchScholarlyArticle)),
  );
  faas.post<PostInitialiseScholarlyArticleRequest>(
    '/articles',
    withDatabase(connectionString, 'periodicals', withSession(initialiseScholarlyArticle)),
  );
  faas.put<PostUpdatePeriodicalRequest>(
    '/articles/:articleId',
    withDatabase(connectionString, 'periodicals', withSession(updateScholarlyArticle)),
  );
  faas.post<PostGenerateUploadUrlForScholarlyArticleRequest>(
    '/articles/:articleId/upload',
    withDatabase(connectionString, 'periodicals', withSession(generateArticleUploadUrl)),
  );
  faas.post<PostPublishScholarlyArticleRequest>(
    '/articles/:articleId/publish',
    withDatabase(connectionString, 'periodicals', withSession(publishScholarlyArticle)),
  );
  faas.get('/:periodicalId', withDatabase(connectionString, 'periodicals', withSession(fetchPeriodical)));
  faas.get(
    '/:periodicalId/exists',
    withDatabase(connectionString, 'periodicals', isPeriodicalSlugAvailable),
  );
  faas.get(
    '/:periodicalId/articles',
    withDatabase(connectionString, 'periodicals', fetchPeriodicalContents),
  );
  faas.post<PostCreatePeriodicalRequest>(
    '/',
    withDatabase(connectionString, 'periodicals', withSession(initialisePeriodical)),
  );
  faas.post<PostSubmitScholarlyArticleRequest>(
    '/:periodicalId/submit/:articleId',
    withDatabase(connectionString, 'periodicals', withSession(submitScholarlyArticle)),
  );
  faas.post<PostGenerateUploadUrlForScholarlyArticleRequest>(
    '/:periodicalId/upload/header',
    withDatabase(connectionString, 'periodicals', withSession(generateAssetUploadUrl)),
  );
  faas.put<PostUpdatePeriodicalRequest>(
    '/:periodicalId',
    withDatabase(connectionString, 'periodicals', withSession(updatePeriodical)),
  );
  faas.post<PostMakePeriodicalPublicRequest>(
    '/:periodicalId/publish',
    withDatabase(connectionString, 'periodicals', withSession(makePublic)),
  );

  faas.default((context: Request<undefined>) => {
    const tail = `/periodicals${context.path}`;

    return Promise.reject(new Error(`Invalid method: ${context.method} ${tail}`));
  });
};
